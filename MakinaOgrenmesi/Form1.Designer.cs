﻿namespace MakinaOgrenmesi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ogretmen = new System.Windows.Forms.RichTextBox();
            this.Ogrenci = new System.Windows.Forms.RichTextBox();
            this.dosyaSec = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.metin = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Ogretmen
            // 
            this.Ogretmen.Font = new System.Drawing.Font("Minion Pro", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ogretmen.ForeColor = System.Drawing.Color.Black;
            this.Ogretmen.Location = new System.Drawing.Point(349, 29);
            this.Ogretmen.Name = "Ogretmen";
            this.Ogretmen.Size = new System.Drawing.Size(337, 240);
            this.Ogretmen.TabIndex = 0;
            this.Ogretmen.Text = "";
            // 
            // Ogrenci
            // 
            this.Ogrenci.Font = new System.Drawing.Font("Minion Pro", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ogrenci.Location = new System.Drawing.Point(349, 295);
            this.Ogrenci.Name = "Ogrenci";
            this.Ogrenci.Size = new System.Drawing.Size(337, 112);
            this.Ogrenci.TabIndex = 1;
            this.Ogrenci.Text = "";
            // 
            // dosyaSec
            // 
            this.dosyaSec.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold);
            this.dosyaSec.Location = new System.Drawing.Point(123, 12);
            this.dosyaSec.Name = "dosyaSec";
            this.dosyaSec.Size = new System.Drawing.Size(86, 25);
            this.dosyaSec.TabIndex = 2;
            this.dosyaSec.Text = "Dosya Seç";
            this.dosyaSec.UseVisualStyleBackColor = true;
            this.dosyaSec.Click += new System.EventHandler(this.dosyaSec_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(349, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kelime Sayıları";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(349, 274);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sonuç";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(12, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Metin";
            // 
            // metin
            // 
            this.metin.Font = new System.Drawing.Font("Minion Pro", 9.749999F);
            this.metin.Location = new System.Drawing.Point(15, 52);
            this.metin.Name = "metin";
            this.metin.Size = new System.Drawing.Size(322, 354);
            this.metin.TabIndex = 8;
            this.metin.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 456);
            this.Controls.Add(this.metin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dosyaSec);
            this.Controls.Add(this.Ogrenci);
            this.Controls.Add(this.Ogretmen);
            this.Font = new System.Drawing.Font("Minion Pro", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox Ogretmen;
        private System.Windows.Forms.RichTextBox Ogrenci;
        private System.Windows.Forms.Button dosyaSec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox metin;
    }
}


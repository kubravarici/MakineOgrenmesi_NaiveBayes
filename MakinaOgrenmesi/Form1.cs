﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace MakinaOgrenmesi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dosyaSec_Click(object sender, EventArgs e)
        {
            metin.Clear();
            Ogretmen.Clear();
            Ogrenci.Clear();

            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Text Dosyası |*.txt";
            file.FilterIndex = 2;
            file.ShowDialog();

            // Dosyamızı okuyacak.
            string dosyaYolu = file.FileName.ToString();
            file.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"; //Seçilebilecek formatları belirtttik. 

            file.InitialDirectory = "c:\\"; //Gözat a basınca ilk açılacak dizin 

            file.FilterIndex = 2;//2 dosya filtrelemesi yaptık 

            file.RestoreDirectory = true; //En son açılan dizi gözata bastığı zaman tekrar açsınmı evet dedik.
            //MessageBox.Show(dosyaYolu);
            StreamReader oku;

            // Belirtmiş olduğum yoldaki dosyayı açacak. 
            /* NOT: @ bu işareti koymamın nedeni \\ 2 defa bundan 
            yapmamak içindir. */
            oku = File.OpenText(dosyaYolu);
            analizEt(dosyaYolu); ;
            string yazi;

            // Satır boş olana kadar okumaya devam eder.
            while ((yazi = oku.ReadLine()) != null)
            {
                // Listbox'ı .txt içeriği ile doldur.
                metin.AppendText(yazi.ToString());
            }

            // Okumayı kapat.
            oku.Close();
        }
        string[] gereksizKelimeler = { "ve", "ile", "ancak", "kadar", "of", "da", "de", "  ", "   " };

        private void analizEt(string dosyaYolu)
        {
            string[] pozitifVocabulary = new string[5000];
            double[] pozitifFrequency = new double[5000];
            double[] pozitifOranlar = new double[5000];
            int pozitifIndex = 0;

            string[] negatifVocabulary = new string[5000];
            double[] negatifFrequency = new double[5000];
            double[] negatifOranlar = new double[5000];
            int negatifIndex = 0;

            string[] notrVocabulary = new string[5000];
            double[] notrFrequency = new double[5000];
            double[] notrOranlar = new double[5000];
            int notrIndex = 0;

            
            int x = 0;//her 10 dosyada bir konu değişmesi için
            for (x = 0; x < 15; x += 5)//40 tane metin
            {
                string[,] KelimelerMatrisi = new string[5, 500];
                double[,] SayacMatrisi = new double[5, 500];
                int m = 0;
                for (int d = x; d < x + 5; d++)
                {

                    string dosyayolu = @"C:\Users\Hp\Desktop\C# Projeler\MakinaOgrenmesi\Train\";
                    string dosya = dosyayolu + "Train_" + (d + 1) + ".txt";

                    FileStream fs = new FileStream(dosya, FileMode.Open, FileAccess.Read);
                    StreamReader sw = new StreamReader(fs);
                    char[] ayraclar = { ' ', '.', ',', ':', ';', '\t', '\n', '?', '!' };
                    string yazi = sw.ReadToEnd().ToLower();
                    string[] metin;

                    metin = yazi.Split(ayraclar);//metnin kelimelere olduğu gibi ayrılmış hali
                    string[] kelimeler = metin.Distinct().ToArray();//tekrarsız kelimeler

                    for (int i = 0; i < kelimeler.Length; i++)
                    {
                        double sayac = 0;
                        string temp = kelimeler[i];
                        for (int j = 0; j < metin.Length; j++)
                        {
                            if (temp == metin[j])
                            {
                                sayac++;
                            }
                        }

                        SayacMatrisi[m, i] = sayac;
                        KelimelerMatrisi[m, i] = kelimeler[i];
                    }
                    m++;
                }
                if (x == 0)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (SayacMatrisi[i, j] != 0 && KelimelerMatrisi[i, j] != " ")
                            {
                                pozitifVocabulary[pozitifIndex] = KelimelerMatrisi[i, j];
                                pozitifFrequency[pozitifIndex] = SayacMatrisi[i, j];
                                pozitifIndex++;
                            }
                        }
                    }

                    for (int i = 0; i < pozitifIndex; i++)
                    {
                        for (int j = i + 1; j < pozitifIndex; j++)
                        {
                            if (pozitifVocabulary[i] == pozitifVocabulary[j])
                            {
                                pozitifVocabulary[j] = "";
                                pozitifFrequency[i] += pozitifFrequency[j];
                                pozitifFrequency[j] = 0;
                            }
                        }
                        pozitifOranlar[i] = pozitifFrequency[i] / pozitifIndex;
                    }
                }

                if (x == 5)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (SayacMatrisi[i, j] != 0 && KelimelerMatrisi[i, j] != "")
                            {
                                negatifVocabulary[negatifIndex] = KelimelerMatrisi[i, j];
                                negatifFrequency[negatifIndex] = SayacMatrisi[i, j];
                                negatifIndex++;
                            }
                        }
                    }

                    for (int i = 0; i < negatifIndex; i++)
                    {
                        for (int j = i + 1; j < negatifIndex; j++)
                        {
                            if (negatifVocabulary[i] == negatifVocabulary[j])
                            {
                                negatifVocabulary[j] = "";
                                negatifFrequency[i] += negatifFrequency[j];
                                negatifFrequency[j] = 0;
                            }
                        }
                        negatifOranlar[i] = negatifFrequency[i] / negatifIndex;
                    }
                }

                if (x == 10)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (SayacMatrisi[i, j] != 0 && KelimelerMatrisi[i, j] != "")
                            {
                                notrVocabulary[notrIndex] = KelimelerMatrisi[i, j];
                                notrFrequency[notrIndex] = SayacMatrisi[i, j];
                                notrIndex++;
                            }
                        }
                    }

                    for (int i = 0; i < notrIndex; i++)
                    {
                        for (int j = i + 1; j < notrIndex; j++)
                        {
                            if (notrVocabulary[i] == notrVocabulary[j])
                            {
                                notrVocabulary[j] = "";
                                notrFrequency[i] += notrFrequency[j];
                                notrFrequency[j] = 0;
                            }
                        }
                        notrOranlar[i] = notrFrequency[i] / notrIndex;
                    }
                }



            }


            string[] TestVocabulary = new string[1000];
            double[] TestFrequency = new double[1000];
            string[] KelimelerMatrisiTest = new string[5000];
            double[] SayacMatrisiTest = new double[5000];


            string dosyatest = dosyaYolu;
            FileStream fstest = new FileStream(dosyatest, FileMode.Open, FileAccess.Read);
            StreamReader swtest = new StreamReader(fstest);
            string yazitest = swtest.ReadToEnd().ToLower();

            string[] metintest;
            char[] ayraclartest = { ' ', '.', ',', ':', ';', '\t', '\n', '?', '!' };
            metintest = yazitest.Split(ayraclartest);
            string[] kelimelertest = metintest.Distinct().ToArray();

            for (int i = 0; i < kelimelertest.Length; i++)
            {
                string temp = kelimelertest[i];
                double sayac = 0;

                for (int j = 0; j < metintest.Length; j++)
                {
                    if (temp == metintest[j])
                    {
                        sayac++;
                    }
                }
                for (int h = 0; h < gereksizKelimeler.Length; h++)//propositions ları attığımız diziyle metindeki kelimeleri karşılaştırıyoruz.
                {
                    if (kelimelertest[i] == gereksizKelimeler[h])//eğer metinde propositionslardan herhangi biri varsa vocablarye eklemeden diğer adıma atlıyoruz.
                    {
                        i++;
                        break;

                    }


                }
                SayacMatrisiTest[i] = sayac;
                KelimelerMatrisiTest[i] = kelimelertest[i];
            }

            int indextest = 0;

            for (int i = 0; i < 500; i++)
            {
                if (SayacMatrisiTest[i] != 0 && KelimelerMatrisiTest[i] != "")
                {
                    TestVocabulary[indextest] = KelimelerMatrisiTest[i];
                    TestFrequency[indextest] = SayacMatrisiTest[i];
                    indextest++;
                }
            }

            for (int i = 0; i < indextest; i++)
            {
                for (int j = i + 1; j < indextest; j++)
                {
                    if (TestVocabulary[i] == TestVocabulary[j])
                    {
                        TestVocabulary[j] = "";
                    }
                }
                Ogretmen.AppendText(TestVocabulary[i] + "   :" + TestFrequency[i] + "\n");
            }

            double pozitif = 1;
            double negatif = 1;
            double notr = 1;


            for (int i = 0; i < indextest; i++)
            {
                for (int j = 0; j < pozitifIndex; j++)
                {
                    if (TestVocabulary[i] == pozitifVocabulary[j] && pozitifOranlar[j] != 0)
                    {
                        pozitif *= pozitifOranlar[j];
                    }
                }
            }
            for (int i = 0; i < indextest; i++)
            {
                for (int j = 0; j < negatifIndex; j++)
                {
                    if (TestVocabulary[i] == negatifVocabulary[j] && negatifOranlar[i] != 0)
                    {
                        negatif *= negatifOranlar[j];
                    }
                }
            }
            for (int i = 0; i < indextest; i++)
            {
                for (int j = 0; j < notrIndex; j++)
                {
                    if (TestVocabulary[i] == notrVocabulary[j] && notrOranlar[i] != 0)
                    {
                        notr *= notrOranlar[j];
                    }
                }
            }


            Ogrenci.AppendText("Pozitif olma olasılığı : " + pozitif + "\n");
            Ogrenci.AppendText("Negatif olma olasılığı : " + negatif + "\n");
            Ogrenci.AppendText("Notr olma olasılığı : " + notr + "\n");


            double[] dizi = { pozitif, negatif, notr };
            double min = pozitif;
            int k;
            for (k = 0; k < dizi.Length; k++)
            {
                if (dizi[k] < min)
                {
                    min = dizi[k];
                }
            }

            if (min == pozitif)
            {
                Ogrenci.AppendText("\nBu döküman POZITIF sınıfına aittir. \n");
            }
            else if (min == negatif)
            {
                Ogrenci.AppendText("\nBu döküman NEGATIF sınıfına aittir. \n");
            }
            else if (min == notr)
            {
                Ogrenci.AppendText("\nBu döküman NOTR sınıfına aittir. \n");
            }

        }
    }
}


Tek k�t� al��kanl���m sensin.
Gidenler gitti... Onlara y�kledi�imiz anlamlar kald� sadece. 
Ve bir g�n biri i�in �ok yoruldum, ba�ka hi� kimse i�in aya�a kalkacak derman�m kalmad�. 
''�yle biri de�il'' diye ba�lad���m c�mlelerden utan�yorum.
Ya�ayamad���m �eyleri �ok �zl�yorum.
Tam olarak olmak istedi�im yerde de�ilim.
�l�m�n so�uk sesiyle ���yoruz �u an.
Seninle bir b�t�n olamad�k ya, parampar�ay�m herkesle.
Bazen �yle oluyor ki; yan�mda kendimi bile istemiyorum.
M�mk�n olmayan �eylere a��k, di�erlerine k�r gibiyiz.
Hayat bilinmez yolculuklara do�ru giden bir gemi, ve ben seni d���nd�k�e o gemiyi bat�r�yorum.
Umursamazca ya��yorum darmada��n, her par�am bir yerlerde yorgunum ve bitkin.
��imde diri diri bo�du�um c�mleler var s�yleyemediklerimin katiliyim.
G�z�m�z g�rmesin diye sa�a sola sakl�yoruz ve buna unutmak diyoruz.
Mutsuzluktan �lm�yorsun, ama s�r�nmemek elde de�il.
�nsanlar gider ve sen onlara y�kledi�in anlamlar�n alt�nda kal�rs�n.
Hayat k�sa, geceler �ok uzun.
Umutsuz hayallerime tan�kl�k eden yast���ma sor inanm�yorsan.
�lmem i�in dua et, ��nk� ben ya�ad�k�a korkacaks�n.
Ve bir g�n birine hayallerini teslim edersin, o k�rmaktan �ekinmez hayal k�r�kl���n olarak kal�r sadece.
D�nyan�n en zor �eyiydi belki; i�in ba��r�rken, dudaklar�n� susturmak.
G�ne� birazdan do�acak ama bizim �st�m�ze de�il biz hep karanl�k kalaca��z seninle.
G�zlerinin ard�ndaki ki�iyi severim ben, g�zlerini de�il.
�nsan olmay�, insanlara anlatmaktan b�kt�m.
Sen bende �ld�n zaten cenazeni kald�racak bir duygum bile yok.
Sonu olmayan bir denizi kar��ma ald�m ben dalgalar ve f�rt�na umrumda de�il.
Ve bir g�n biri i�in �ok yoruldum.
Seni seviyor olmam, yan�mda istedi�im anlam�na gelmez ��nk� yoklu�unla varl���ndan daha iyi anla��yorum.
Ve bazen sadece gitmek istersin nereye oldu�unu bilmeden hatta bunu hi� sorgulamadan sadece gitmek.
Haf�zas� g��l� insanlar�n i�leri yaralarla doludur her �eyi hat�rlamayacak bir haf�zam�n olmas�n� dilerdim.



